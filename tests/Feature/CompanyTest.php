<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Company;

class CompanyTest extends TestCase
{
    use RefreshDatabase;

    public function test_authenticated_users_can_see_company_list()
    {
        $this->actingAs(factory(User::class)->create());

        $company = factory('App\Company')->create();

        $response = $this->get('/admin/companies');

        $response->assertSee($company->name);
    }

    /** @test */
    public function authenticated_users_can_create_new_company(){
        $this->actingAs(factory(User::class)->create());

        $company = factory('App\Company')->make();

        $response = $this->post('/admin/companies', $company->toArray());

        $this->assertCount(1, Company::all());
    }

    /** @test */
    public function unauthenticated_users_cannot_create_a_new_company()
    {
        $company = factory('App\Company')->make();
        $response = $this->post('/admin/companies', $company->toArray())->assertRedirect('/login');
    }

    /** @test */
    public function company_requires_a_name(){

        $this->actingAs(factory('App\User')->create());
    
        $company = factory('App\Company')->make(['name' => null]);
    
        $this->post('/admin/companies',$company->toArray())
                ->assertSessionHasErrors('name');
    }

    /** @test */
    public function authorized_user_can_update_a_company(){
        $this->actingAs(factory('App\User')->create());

        $company = factory('App\Company')->create();
        $company->name = "Updated Name";

        $this->put('/admin/companies/'.$company->id, $company->toArray());

        $this->assertDatabaseHas('companies',['id'=> $company->id , 'name' => 'Updated Name']);

    }

    /** @test */
    public function unauthorized_user_cannot_update_a_company(){
        $company = factory('App\Company')->create();
        $company->name = "Updated Name";

        $response = $this->put('/admin/companies/'.$company->id, $company->toArray());
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authorized_user_can_delete_a_company(){

        $this->actingAs(factory('App\User')->create());

        $company = factory('App\Company')->create();
        $this->delete('/admin/companies/'.$company->id);
        $this->assertDatabaseMissing('companies',['id'=> $company->id]);

    }



}
