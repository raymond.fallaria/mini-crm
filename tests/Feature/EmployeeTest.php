<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Employee;

class EmployeeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticated_users_can_see_employee_list()
    {
        $this->actingAs(factory(User::class)->create());

        $employee = factory('App\Employee')->create();

        $response = $this->get('/admin/employees');

        $response->assertSee($employee->first_name);
    }

    /** @test */
    public function authenticated_users_can_create_new_employee(){

        $this->actingAs(factory(User::class)->create());

        $company = factory('App\Company')->create();

        $employee = factory('App\Employee')->make(['company_id' => $company->id]);

        $response = $this->post('/admin/employees', $employee->toArray());

        $this->assertCount(1, Employee::all());
    }

     /** @test */
     public function unauthenticated_users_cannot_create_a_new_employee()
     {
         $employee = factory('App\Employee')->make();
         $response = $this->post('/admin/employees', $employee->toArray())->assertRedirect('/login');
     }

    /** @test */
    public function employee_requires_first_name(){

        $this->actingAs(factory('App\User')->create());
    
        $employee = factory('App\Employee')->make(['first_name' => null]);
    
        $this->post('/admin/employees',$employee->toArray())
                ->assertSessionHasErrors('first_name');
    }

    /** @test */
    public function employee_requires_last_name(){

        $this->actingAs(factory('App\User')->create());
    
        $employee = factory('App\Employee')->make(['last_name' => null]);
    
        $this->post('/admin/employees',$employee->toArray())
                ->assertSessionHasErrors('last_name');
    }

     /** @test */
     public function authorized_user_can_update_an_employee(){
        $this->actingAs(factory('App\User')->create());

        $employee = factory('App\Employee')->create();
        $employee->first_name = "Updated First Name";

        $this->put('/admin/employees/'.$employee->id, $employee->toArray());

        $this->assertDatabaseHas('employees',['id'=> $employee->id , 'first_name' => 'Updated First Name']);

    }

    /** @test */
    public function unauthorized_user_cannot_update_an_employee(){
        $employee = factory('App\Employee')->create();
        $employee->first_name = "Updated First Name";

        $response = $this->put('/admin/employees/'.$employee->id, $employee->toArray());
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authorized_user_can_delete_an_employee(){

        $this->actingAs(factory('App\User')->create());

        $employee = factory('App\Employee')->create();
        $this->delete('/admin/employees/'.$employee->id);
        $this->assertDatabaseMissing('employees',['id'=> $employee->id]);

    }

}
