<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'List of Employees';
        
        return view('admin.employees.index', ['employees' => Employee::with('company')->paginate(10), 'pageTitle' => $pageTitle]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = 'Create Employee';
        $companies = Company::get();
        return view('admin.employees.form', ['pageTitle' => $pageTitle, 'companies' => $companies]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
        ]);

        DB::transaction(function () use ($request){
            $employee = new Employee([
            "first_name" => $request->get('first_name'),
            "last_name" => $request->get('last_name'),
            "company_id" => $request->get('company_id'),
            "email" => $request->get('email'),
            "phone" => $request->get('phone'),
        ]);
            $employee->save();
        });

        return redirect(route('employees.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        $pageTitle = 'Edit Employee';
        $companies = Company::get();

        return view('admin.employees.form', ['employee' => $employee, 'companies' => $companies, 'pageTitle' => $pageTitle]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
        ]);

        DB::transaction(function () use ($request, $id){
            $employee = Employee::find($id);
            $employee->first_name = $request->get('first_name');
            $employee->last_name = $request->get('last_name');
            $employee->company_id = $request->get('company_id');
            $employee->email = $request->get('email');
            $employee->phone = $request->get('phone');
            $employee->save();
        });

        return redirect(route('employees.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id){
            Employee::find($id)->delete($id);
        });
    }
}
