<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use App\Mail\NewCompanyNotification;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'List of Companies';
        return view('admin.companies.index', ['companies' => Company::paginate(10), 'pageTitle' => $pageTitle]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = 'Create Company';
        return view('admin.companies.form', ['pageTitle' => $pageTitle]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'logo' => 'dimensions:min_width=100,min_height=100',
        ]);

        DB::transaction(function () use ($request){
            $request->file('logo') ? $request->file('logo')->store('public') : '';

            $company = new Company([
                "name" => $request->get('name'),
                "website" => $request->get('website'),
                "email" => $request->get('email'),
                "logo" => $request->file('logo') ? $request->file('logo')->hashName() : ''
            ]);
            $company->save(); 

            $email = $request->get('email');
            Mail::to($email)->send(new NewCompanyNotification);
        });
        return redirect(route('companies.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        $pageTitle = 'Edit Company';

        return view('admin.companies.form', ['company' => $company, 'pageTitle' => $pageTitle]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
            'name' => 'required',
            'logo' => 'dimensions:min_width=100,min_height=100',
        ]);

        DB::transaction(function () use ($request, $id){
             $company = Company::find($id);

            $request->file('logo') ? $request->file('logo')->store('public') : '';

            $company->name = $request->get('name');
            $company->website = $request->get('website');
            $company->email = $request->get('email');
            if ($company->logo) {
                $logoInput = $company->logo;
            } else {
                $logoInput = '';
            }
            $company->logo = $request->file('logo') ? $request->file('logo')->hashName() : $logoInput;
            $company->save();
        });

        return redirect(route('companies.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            Company::find($id)->delete($id);
        });
    }

}
