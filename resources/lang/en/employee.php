<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Company Pages
    |--------------------------------------------------------------------------
    */

    'title' => 'Employees',
    'title_index' => 'List of Employees',
    'title_create' => 'Create Employee',
    'title_edit' => 'Edit Employee',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'submit' => 'Submit',
];
