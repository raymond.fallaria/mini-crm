<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Company Pages
    |--------------------------------------------------------------------------
    */

    'title' => 'Companies',
    'title_index' => 'List of Companies',
    'title_create' => 'Create Company',
    'title_edit' => 'Edit Company',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'submit' => 'Submit',

];
