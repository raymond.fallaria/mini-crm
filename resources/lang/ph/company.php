<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Company Pages
    |--------------------------------------------------------------------------
    */

    'title' => 'Kumpanya',
    'title_index' => 'Listahan ng mga Kumpanya',
    'title_create' => 'Lumikha ng Kumpanya',
    'title_edit' => 'I-edit ang Kumpanya',
    'edit' => 'I-edit',
    'delete' => 'Tanggalin',
    'submit' => 'Ipasa',

];
