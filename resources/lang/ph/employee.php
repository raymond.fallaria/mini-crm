<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Company Pages
    |--------------------------------------------------------------------------
    */

    'title' => 'Mga empleyado',
    'title_index' => 'Listahan ng mga empleyado',
    'title_create' => 'Lumikha ng empleyado',
    'title_edit' => 'I-edit ang empleyado',
    'edit' => 'I-edit',
    'delete' => 'Tanggalin',
    'submit' => 'Ipasa',
];
