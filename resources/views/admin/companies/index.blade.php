@extends('adminlte::page')

@section('title', __('company.title'))

@section('content_header')
    <h1>@lang('company.title_index')</h1>
@stop

@section('content')
<a class="btn btn-primary" href="{{ route('companies.create') }}">@lang('company.title_create')</a>
    <table id="myTable"  class="stripe">
        <thead>
    <tr>
            <td>Name</td>
            <td>Email</td>
            <td>Website</td>
            <td>Logo</td>
            <td>Action</td>
    </tr>
    </thead>
    <tbody>
    @foreach($companies as $company)
        <tr>
            <td>{{$company->name}}</td>
            <td>{{$company->email}}</td>
            <td>{{$company->website}}</td>
            <td> @if($company->logo)<img src="/storage/{{$company->logo}}" style="height: 100px;">@endif</td>
            <td><a class="btn btn-success" href="{{ route('companies.edit', ['id'=>$company->id]) }}">@lang('company.edit')</a> 
            <button class="btn btn-danger deleteBtn" data-id="{{$company->id}}">@lang('company.delete')</button></td>
        </tr>
    @endforeach
    </tbody>
    </table>
   

    {{ $companies->links() }}
@stop

@section('js')
<script>
$(document).ready( function () {
    $('#myTable').DataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "searching": false, 
        "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ],
    });

    $('.deleteBtn').click(function(){
        Swal.fire({
            title: "{{__('validation.confirmation')}}",
            text: "{{__('validation.confirmation_sub')}}",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "{{__('validation.delete_btn')}}",
            cancelButtonText: "{{__('validation.cancel_btn')}}"
            }).then((result) => {
            if (result.value) {
                var id = $(this).data("id");
                $.ajax(
                {
                    url: "companies/"+id,
                    type: 'DELETE',
                    data: {
                        "id": id,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (){
                        Swal.fire(
                        "{{__('validation.deleted')}}",
                        "{{__('validation.deleted_sub')}}",
                        'success'
                        ).then(function(){
                            window.location = '/admin/companies';
                        })
                    }
                });
           
            }
        })
    })

} );
</script>
@stop