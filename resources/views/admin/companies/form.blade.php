@extends('adminlte::page')

@section('title', isset($company) ? __('company.title_edit') : __('company.title_create'))

@section('content_header')
    <h1>{{isset($company) ? __('company.title_edit') : __('company.title_create')}}</h1>
@stop

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li> @lang($error)</li>
            @endforeach
        </ul>
    </div>
@endif
<form method="post" action="{{isset($company) ? route('companies.update', ['id'=>$company->id]) : route('companies.store')}}" enctype="multipart/form-data">
@csrf
@if(isset($company))
@method('PATCH')
@endif
<div class="form-group">
    <label for="nameInput">Name <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="nameInput" placeholder="Name" name="name" value="{{$company->name ?? ''}}">
  </div>
  <div class="form-group">
    <label for="emailInput">Email</label>
    <input type="email" class="form-control" id="emailInput" name="email" placeholder="Email " value="{{$company->email ?? ''}}">
  </div>
  <div class="form-group">
    <label for="websiteInput">Website</label>
    <input type="text" class="form-control" id="websiteInput" name="website" placeholder="Website" value="{{$company->website ?? ''}}"> 
  </div>
  <div class="form-group">
    <label for="logoInput">Upload Logo (minimum 100x100)</label>
  
    <input name="logo" type="file" class="form-control-file" id="logoInput">
    @if(isset($company))
        @if($company->logo)
          <img class="img-thumbnail" src="/storage/{{$company->logo}}">
        @endif
    @endif
  </div>
  <button type="submit" class="btn btn-primary">@lang('company.submit')</button>

</form>
@stop