@extends('adminlte::page')

@section('title', isset($employee) ? __('employee.title_edit') : __('employee.title_create'))

@section('content_header')
<h1>{{isset($employee) ? __('employee.title_edit') : __('employee.title_create')}}</h1>
@stop

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form method="post" action="{{isset($employee) ? route('employees.update', ['id'=>$employee->id]) : route('employees.store')}}" enctype="multipart/form-data">
@csrf
@if(isset($employee))
@method('PATCH')
@endif
<div class="form-group">
    <label for="firstNameInput">First Name <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="firstNameInput" placeholder="First Name" name="first_name" value="{{$employee->first_name ?? ''}}">
  </div>
  <div class="form-group">
    <label for="lastNameInput">Last Name <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="lastNameInput" placeholder="Last Name" name="last_name" value="{{$employee->last_name ?? ''}}">
  </div>
  <div class="form-group">
    <label for="companyInput">Company {{$employee->company_id ?? ''}}</label>
    <select class="form-control" id="companyInput" name="company_id">
      <option value="''" disabled selected value>Choose company</option>
      @foreach($companies as $company)
      <option value="{{ $company->id}}" {{ ( isset($employee->company_id) && $employee->company_id == $company->id) ? 'selected' : '' }}>{{$company->name}}</option>
      @endforeach
    </select>
  </div>
  <div class="form-group">
    <label for="emailInput">Email</label>
    <input type="email" class="form-control" id="emailInput" name="email" placeholder="Email " value="{{$employee->email ?? ''}}">
  </div>
  <div class="form-group">
    <label for="phoneInput">Phone <span class="text-danger"></span></label>
    <input type="text" class="form-control" id="phoneInput" placeholder="Enter Phone" name="phone" value="{{$employee->phone ?? ''}}">
  </div>
  <button type="submit" class="btn btn-primary">@lang('employee.submit')</button>

</form>
@stop