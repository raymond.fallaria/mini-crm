@extends('adminlte::page')

@section('title', __('employee.title'))

@section('content_header')
    <h1>@lang('employee.title_index')</h1>
@stop

@section('content')
<a class="btn btn-primary" href="{{ route('employees.create') }}">@lang('employee.title_create')</a>
    <table id="myTable"  class="stripe">
        <thead>
    <tr>
            <td>First Name</td>
            <td>Last Name</td>
            <td>Company</td>
            <td>Email</td>
            <td>Phone</td>
            <td>Action</td>
    </tr>
    </thead>
    <tbody>
    @foreach($employees as $employee)
        <tr>
            <td>{{$employee->first_name}}</td>
            <td>{{$employee->last_name}}</td>
            <td>{{$employee->company ? $employee->company->name : ''}}</td>
            <td>{{$employee->email}}</td>
            <td>{{$employee->phone}}</td>
            <td><a class="btn btn-success" href="{{ route('employees.edit', ['id'=>$employee->id]) }}">@lang('employee.edit')</a> 
            <button class="btn btn-danger deleteBtn" data-id="{{$employee->id}}">@lang('employee.delete')</button></td>
        </tr>
    @endforeach
    </tbody>
    </table>
   

    {{ $employees->links() }}
@stop

@section('js')
<script>
$(document).ready( function () {
    $('#myTable').DataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "searching": false, 
        "columnDefs": [
        {"className": "dt-center", "targets": "_all"}
      ],
    });

    $('.deleteBtn').click(function(){
        Swal.fire({
            title: "{{__('validation.confirmation')}}",
            text: "{{__('validation.confirmation_sub')}}",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "{{__('validation.delete_btn')}}",
            cancelButtonText: "{{__('validation.cancel_btn')}}"
            }).then((result) => {
            if (result.value) {
                var id = $(this).data("id");
                $.ajax(
                {
                    url: "  employees/"+id,
                    type: 'DELETE',
                    data: {
                        "id": id,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (){
                        Swal.fire(
                        "{{__('validation.deleted')}}",
                        "{{__('validation.deleted_sub')}}",
                        'success'
                        ).then(function(){
                            window.location = '/admin/employees';
                        })
                    }
                });
           
            }
        })
    })

} );
</script>
@stop